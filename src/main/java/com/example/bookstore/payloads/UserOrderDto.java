package com.example.bookstore.payloads;

import lombok.Data;

import java.util.List;

@Data
public class UserOrderDto {
    private List<Integer> orders;
}
