package com.example.bookstore.payloads;

import com.example.bookstore.utils.MoneySerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class BookDto {

    private Integer id;
    private String book;
    private String author;

    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal price;

    @JsonProperty("is_recommended")
    private boolean isRecommended;
}
