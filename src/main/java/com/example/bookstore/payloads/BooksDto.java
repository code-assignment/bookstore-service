package com.example.bookstore.payloads;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class BooksDto {
    private List<BookDto> books;
}
