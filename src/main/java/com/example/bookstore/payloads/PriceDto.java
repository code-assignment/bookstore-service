package com.example.bookstore.payloads;

import com.example.bookstore.utils.MoneySerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class PriceDto {
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal price;
}
