package com.example.bookstore.constants;

public class SecurityConstants {
    public static final String KEY = "y/B?E(H+MbQeThWmYq3t6w9z$C&F)J@NcRfUjXn2r4u7x!A%D*G-KaPdSgVkYp3s";
    public static final String HEADER_NAME = "Authorization";
    public static final Long EXPIRATION_TIME = 1000L*60*30;

    public static final String CREATE_USER_URL = "/users";
    public static final String RECEIVE_BOOK_URL = "/books";
}
