package com.example.bookstore.services;

import com.example.bookstore.models.UserEntity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

import static java.util.Collections.emptyList;

@Service
public class CredentialService implements UserDetailsService {

    private final UserService userService;

    public CredentialService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            UserEntity user = userService.getByUsername(username);
            return new User(user.getUsername(), user.getPassword(), emptyList());
        } catch (EntityNotFoundException ex) {
            throw new UsernameNotFoundException(username, ex);
        }
    }
}
