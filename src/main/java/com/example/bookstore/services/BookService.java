package com.example.bookstore.services;

import com.example.bookstore.clients.PublisherClient;
import com.example.bookstore.payloads.BookClientDto;
import com.example.bookstore.payloads.BookDto;
import com.example.bookstore.payloads.BooksDto;
import com.example.bookstore.mappers.BookMapper;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {

    private final PublisherClient publisherClient;

    public BookService(PublisherClient publisherClient) {
        this.publisherClient = publisherClient;
    }

    public BooksDto getBooks() {
        List<BookDto> bookResponseDtoList = BookMapper.INSTANCE.map(publisherClient.getBooks());
        for(BookClientDto bookDto : publisherClient.getBooksRecommendation()) {
            setRecommendedBook(bookResponseDtoList, bookDto);
        }
        return BooksDto
                .builder()
                .books(sortedBookByAlphabet(bookResponseDtoList)).build();
    }

    private List<BookDto> sortedBookByAlphabet(List<BookDto> bookResponseDtoList) {
        return bookResponseDtoList
                .stream()
                .sorted(Comparator.comparing(BookDto::isRecommended, Comparator.reverseOrder()).thenComparing(BookDto::getBook))
                .collect(Collectors.toList());
    }

    private void setRecommendedBook(List<BookDto> bookResponseDtoList, BookClientDto bookDto) {
        bookResponseDtoList
                .stream()
                .filter(book -> bookDto.getId() == book.getId())
                .forEach(book -> book.setRecommended(true));
    }

    public BooksDto getByIds(List<Integer> bookIds) {
        return BooksDto
                .builder()
                .books(filteredBookByIds(bookIds))
                .build();
    }

    private List<BookDto> filteredBookByIds(List<Integer> bookIds) {
        return this.getBooks().getBooks()
                .stream()
                .filter(book -> bookIds.contains(book.getId()))
                .collect(Collectors.toList());
    }

    public void syncBooks() {
        saveBooks(this.getBooks());
    }

    private void saveBooks(BooksDto booksDto) {
        // Do something
        System.out.println("saving... Books");
    }
}
