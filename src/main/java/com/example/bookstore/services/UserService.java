package com.example.bookstore.services;

import com.example.bookstore.exceptions.BusinessException;
import com.example.bookstore.mappers.UserMapper;
import com.example.bookstore.models.UserEntity;
import com.example.bookstore.payloads.CreateUserDto;
import com.example.bookstore.payloads.UserDto;
import com.example.bookstore.repositories.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Date;

@Service
public class UserService {

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    public UserService(UserRepository repository, PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    public UserEntity getByUsername(String username) {
        return repository.findByUsername(username)
                .orElseThrow(() -> new EntityNotFoundException(String.format("Username %s not found.", username)));
    }

    public void createUser(CreateUserDto userRequestDto) {
        String username = userRequestDto.getUsername();
        String[] name = getName(username);

        UserEntity user = new UserEntity();
        user.setUsername(username);
        user.setPassword(getPassword(userRequestDto));
        user.setName(name[0]);
        user.setSurname(name[1]);
        user.setDateOfBirth(userRequestDto.getDateOfBirth());
        user.setCreatedDate(new Date());
        repository.save(user);
    }

    private String[] getName(String username) {
        String[] name = username.split("\\.");
        if(name.length > 2)
            throw new BusinessException("Wrong username pattern. Must be like {name}.{surname}");
        return name;
    }

    private String getPassword(CreateUserDto userRequestDto) {
        return passwordEncoder.encode(userRequestDto.getPassword());
    }

    public void deleteUser(String username) {
        UserEntity user = this.getByUsername(username);
        repository.delete(user);
    }

    public UserDto getUserWithOrders(String username) {
        UserEntity user = this.getByUsername(username);
        UserDto userDto = UserMapper.INSTANCE.toDto(user);
        userDto.setBooks(UserMapper.INSTANCE.map(user.getOrders()));
        return userDto;
    }
}
