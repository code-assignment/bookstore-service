package com.example.bookstore.services;

import com.example.bookstore.exceptions.BusinessException;
import com.example.bookstore.models.OrderEntity;
import com.example.bookstore.models.UserEntity;
import com.example.bookstore.payloads.BookDto;
import com.example.bookstore.payloads.BooksDto;
import com.example.bookstore.payloads.PriceDto;
import com.example.bookstore.payloads.UserOrderDto;
import com.example.bookstore.repositories.OrderRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;

@Service
public class OrderService {

    private final OrderRepository orderRepository;

    private final UserService userService;
    private final BookService bookService;

    public OrderService(OrderRepository orderRepository, UserService userService, BookService bookService) {
        this.orderRepository = orderRepository;
        this.userService = userService;
        this.bookService = bookService;
    }

    @Transactional
    public PriceDto createOrder(String username, @RequestBody UserOrderDto userOrderRequestDto) {
        validateOrders(userOrderRequestDto);
        UserEntity user = userService.getByUsername(username);
        BooksDto books = bookService.getByIds(userOrderRequestDto.getOrders());

        PriceDto totalPrice = PriceDto.builder().price(BigDecimal.ZERO).build();

        for (BookDto book : books.getBooks()) {
            validateOrder(book);
            OrderEntity order = saveOrder(user, book);
            sumOrderPrice(totalPrice, order);
        }
        return totalPrice;
    }

    private void validateOrders(UserOrderDto userOrderRequestDto) {
        if(userOrderRequestDto.getOrders().size() == 0) {
            throw new BusinessException("Must order at least one.");
        }
    }

    private void validateOrder(BookDto book) {
        if(orderRepository.findByBookId(book.getId()).isPresent())
            throw new BusinessException(String.format("Already order this book : [id=%s, book=%s]", book.getId(), book.getBook()));
    }

    private OrderEntity saveOrder(UserEntity user, BookDto book) {
        OrderEntity order = new OrderEntity();
        order.setBookId(book.getId());
        order.setBookName(book.getBook());
        order.setBookAuthor(book.getAuthor());
        order.setPrice(book.getPrice());
        order.setRecommended(book.isRecommended());
        order.setUser(user);
        order.setCreatedDate(new Date());
        orderRepository.save(order);
        return order;
    }

    private void sumOrderPrice(PriceDto totalPrice, OrderEntity order) {
        BigDecimal price = totalPrice.getPrice().add(order.getPrice());
        totalPrice.setPrice(price);
    }
}
