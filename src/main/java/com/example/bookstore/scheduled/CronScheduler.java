package com.example.bookstore.scheduled;

import com.example.bookstore.services.BookService;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
public class CronScheduler {

    private final BookService service;

    public CronScheduler(BookService service) {
        this.service = service;
    }

    @Scheduled(cron = "${scheduled.cron.sync-book.expression}")
    public void syncBookTask() {
        service.syncBooks();
    }
}
