package com.example.bookstore.controllers;

import com.example.bookstore.payloads.UserOrderDto;
import com.example.bookstore.payloads.CreateUserDto;
import com.example.bookstore.services.OrderService;
import com.example.bookstore.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final OrderService orderService;

    public UserController(UserService userService, OrderService orderService) {
        this.userService = userService;
        this.orderService = orderService;
    }

    @GetMapping
    public ResponseEntity<?> users(Authentication authentication) {
        return ResponseEntity.ok().body(userService.getUserWithOrders(authentication.getName()));
    }

    @DeleteMapping
    public ResponseEntity<?> deleteUser(Authentication authentication) {
        userService.deleteUser(authentication.getName());
        return ResponseEntity.ok("Delete user success.");
    }

    @PostMapping
    public ResponseEntity<?> createUser(@RequestBody CreateUserDto userRequestDto) {
        userService.createUser(userRequestDto);
        return ResponseEntity.ok("Create user success.");
    }

    @PostMapping("/orders")
    public ResponseEntity<?> createOrders(Authentication authentication, @RequestBody UserOrderDto userOrderRequestDto) {
        return ResponseEntity.ok().body(orderService.createOrder(authentication.getName(), userOrderRequestDto));
    }
}
