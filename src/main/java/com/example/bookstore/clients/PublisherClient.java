package com.example.bookstore.clients;

import com.example.bookstore.payloads.BookClientDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(value = "publisher", url = "${client.publisher}")
public interface PublisherClient {

    @GetMapping("/books")
    List<BookClientDto> getBooks();

    @GetMapping("/books/recommendation")
    List<BookClientDto> getBooksRecommendation();
}
