package com.example.bookstore.mappers;

import com.example.bookstore.payloads.BookClientDto;
import com.example.bookstore.payloads.BookDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface BookMapper {

    BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);

    List<BookDto> map(List<BookClientDto> books);

    default BookDto map(BookClientDto book) {
        return BookDto
                .builder()
                .id(book.getId())
                .book(book.getBookName())
                .author(book.getAuthorName())
                .price(book.getPrice())
                .build();
    }
}
