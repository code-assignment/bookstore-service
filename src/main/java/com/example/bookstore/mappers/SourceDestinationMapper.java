package com.example.bookstore.mappers;

import java.util.List;

public interface SourceDestinationMapper<Model, Dto> {
    Dto toDto(Model source);
    List<Dto> toDto(List<Model> source);
    Model toModel(Dto source);
    List<Model> toModel(List<Dto> source);
}
