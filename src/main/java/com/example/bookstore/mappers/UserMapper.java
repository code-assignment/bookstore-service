package com.example.bookstore.mappers;

import com.example.bookstore.models.OrderEntity;
import com.example.bookstore.models.UserEntity;
import com.example.bookstore.payloads.BookDto;
import com.example.bookstore.payloads.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface UserMapper extends SourceDestinationMapper<UserEntity, UserDto> {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    default List<BookDto> map(List<OrderEntity> orders) {
        List<BookDto> books = new ArrayList<>(orders.size());

        for(OrderEntity order : orders) {
            books.add(map(order));
        }
        return books;
    }

    default BookDto map(OrderEntity order) {
        return BookDto
                .builder()
                .id(order.getBookId())
                .book(order.getBookName())
                .author(order.getBookAuthor())
                .price(order.getPrice())
                .isRecommended(order.isRecommended())
                .build();
    }
}
