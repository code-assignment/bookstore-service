SET NAMES utf8;

CREATE DATABASE `bookstore_db`  CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `bookstore_db`;

CREATE TABLE `users` (
    `user_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`username` VARCHAR(100) NOT NULL,
	`password` VARCHAR(200) NOT NULL,
	`name` VARCHAR(100) NOT NULL,
	`surname` VARCHAR(100) NOT NULL,
	`date_of_birth` DATE NOT NULL,
	`created_date` DATETIME NOT NULL,
	PRIMARY KEY (`user_id`),
	CONSTRAINT `uc_username` UNIQUE (`username`)
);

CREATE TABLE `orders` (
	`order_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT UNSIGNED NOT NULL,
	`book_id` INT UNSIGNED NOT NULL,
    `book_name` VARCHAR(200) NOT NULL,
    `book_author` VARCHAR(200) NOT NULL,
    `price` DECIMAL(10,2) NOT NULL,
    `is_recommended` TINYINT UNSIGNED NOT NULL,
	`created_date` DATETIME NOT NULL,
	PRIMARY KEY (`order_id`),
	FOREIGN KEY (`user_id`) REFERENCES `users`(`user_id`)
);