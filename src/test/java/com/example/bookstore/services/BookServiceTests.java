package com.example.bookstore.services;

import com.example.bookstore.clients.PublisherClient;
import com.example.bookstore.payloads.BookClientDto;
import com.example.bookstore.payloads.BookDto;
import com.example.bookstore.payloads.BooksDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BookServiceTests {

    @InjectMocks
    BookService service;

    @Mock
    PublisherClient client;

    @Test
    public void getBookClientTest() {
        List<BookClientDto> bookClientDtos = new ArrayList<>();
        bookClientDtos.add(createBookClientDto(1, "book a", "author a", 100.25));
        bookClientDtos.add(createBookClientDto(2, "book b", "author b", 50.50));
        bookClientDtos.add(createBookClientDto(3, "book c", "author c", 35));

        when(client.getBooks()).thenReturn(bookClientDtos);

        assertEquals(3, client.getBooks().size());
        verify(client, times(1)).getBooks();
    }

    @Test
    public void getBooksRecommendationTest() {
        List<BookClientDto> bookClientDtos = new ArrayList<>();
        bookClientDtos.add(createBookClientDto(1, "book a", "author a", 100.25));
        bookClientDtos.add(createBookClientDto(2, "book b", "author b", 50.50));
        bookClientDtos.add(createBookClientDto(3, "book c", "author c", 35));

        when(client.getBooksRecommendation()).thenReturn(bookClientDtos);

        assertEquals(3, client.getBooksRecommendation().size());
        verify(client, times(1)).getBooksRecommendation();
    }

    @Test
    public void getBooksTest() {
        List<BookDto> listExpect = new ArrayList<>();
        listExpect.add(
                BookDto
                        .builder()
                        .id(2)
                        .book("book b")
                        .author("author b")
                        .price(BigDecimal.valueOf(50.50))
                        .isRecommended(true)
                        .build());
        listExpect.add(
                BookDto
                        .builder()
                        .id(1)
                        .book("book a")
                        .author("author a")
                        .price(BigDecimal.valueOf(100.25))
                        .isRecommended(false)
                        .build());
        listExpect.add(
                BookDto
                        .builder()
                        .id(3)
                        .book("book c")
                        .author("author c")
                        .price(BigDecimal.valueOf(35.00))
                        .isRecommended(false)
                        .build());
        BooksDto booksDtoExpect = BooksDto.builder().books(listExpect).build();

        List<BookClientDto> bookRecommendationClientDtos = new ArrayList<>();
        bookRecommendationClientDtos.add(createBookClientDto(2, "book b", "author b", 50.50));

        when(client.getBooksRecommendation()).thenReturn(bookRecommendationClientDtos);

        List<BookClientDto> bookClientDtos = new ArrayList<>();
        bookClientDtos.add(createBookClientDto(1, "book a", "author a", 100.25));
        bookClientDtos.add(createBookClientDto(2, "book b", "author b", 50.50));
        bookClientDtos.add(createBookClientDto(3, "book c", "author c", 35.00));

        when(client.getBooks()).thenReturn(bookClientDtos);

        assertEquals(booksDtoExpect, service.getBooks());
    }

    private BookClientDto createBookClientDto(Integer id, String book, String author, double price) {
        BookClientDto bookClientDto = new BookClientDto();
        bookClientDto.setId(id);
        bookClientDto.setBookName(book);
        bookClientDto.setAuthorName(author);
        bookClientDto.setPrice(BigDecimal.valueOf(price));
        return bookClientDto;
    }

    @Test
    public void setRecommendedBookTest() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        List<BookDto> listExpect = new ArrayList<>();
        listExpect.add(
                BookDto
                        .builder()
                        .id(1)
                        .book("book a")
                        .author("author a")
                        .price(BigDecimal.valueOf(100.25))
                        .isRecommended(false)
                        .build());
        listExpect.add(
                BookDto
                        .builder()
                        .id(2)
                        .book("book b")
                        .author("author b")
                        .price(BigDecimal.valueOf(50.50))
                        .isRecommended(true)
                        .build());
        listExpect.add(
                BookDto
                        .builder()
                        .id(3)
                        .book("book c")
                        .author("author c")
                        .price(BigDecimal.valueOf(35))
                        .isRecommended(false)
                        .build());

        List<BookDto> bookResponseDtoList = new ArrayList<>();
        bookResponseDtoList.add(
                BookDto.builder().id(1).book("book a").author("author a").price(BigDecimal.valueOf(100.25)).build());
        bookResponseDtoList.add(
                BookDto.builder().id(2).book("book b").author("author b").price(BigDecimal.valueOf(50.50)).build());
        bookResponseDtoList.add(
                BookDto.builder().id(3).book("book c").author("author c").price(BigDecimal.valueOf(35)).build());

        BookClientDto bookClientDto = createBookClientDto(2, "book b", "author b", 50.50);

        Method method = service.getClass().getDeclaredMethod("setRecommendedBook", List.class, BookClientDto.class);
        method.setAccessible(true);
        method.invoke(service, bookResponseDtoList, bookClientDto);

        assertEquals(listExpect, bookResponseDtoList);

        bookResponseDtoList.forEach(System.out::println);
    }

    @Test
    public void filteredBookByIdsTest() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        List<BookDto> bookDtosExpect = new ArrayList<>();
        bookDtosExpect.add(createBooksDto(1, "book a", "author a", 100.25, false));
        bookDtosExpect.add(createBooksDto(3, "book c", "author c", 35, false));

        List<BookClientDto> bookClientDtos = new ArrayList<>();
        bookClientDtos.add(createBookClientDto(1, "book a", "author a", 100.25));
        bookClientDtos.add(createBookClientDto(2, "book b", "author b", 50.50));
        bookClientDtos.add(createBookClientDto(3, "book c", "author c", 35));

        when(client.getBooks()).thenReturn(bookClientDtos);

        List<Integer> bookIds = Arrays.asList(1, 3);

        Method method = service.getClass().getDeclaredMethod("filteredBookByIds", List.class);
        method.setAccessible(true);

        List<BookDto> bookDtoList = (List<BookDto>) method.invoke(service, bookIds);

        assertEquals(bookDtosExpect, bookDtoList);

        bookDtoList.forEach(System.out::println);
    }

    private BookDto createBooksDto(Integer id, String book, String author, double price, boolean isRecommended) {
        return BookDto
                .builder()
                .id(id)
                .book(book)
                .author(author)
                .price(BigDecimal.valueOf(price))
                .isRecommended(isRecommended)
                .build();
    }

    @Test
    public void getByIdsTest() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        List<BookClientDto> bookClientDtos = new ArrayList<>();
        bookClientDtos.add(createBookClientDto(1, "book a", "author a", 100.25));
        bookClientDtos.add(createBookClientDto(2, "book b", "author b", 50.50));
        bookClientDtos.add(createBookClientDto(3, "book c", "author c", 35));

        when(client.getBooks()).thenReturn(bookClientDtos);

        Method method = service.getClass().getDeclaredMethod("filteredBookByIds", List.class);
        method.setAccessible(true);

        List<Integer> bookIds = Arrays.asList(1, 3);
        List<BookDto> bookDtoList = (List<BookDto>) method.invoke(service, bookIds);

        BooksDto booksDto = service.getByIds(bookIds);

        assertEquals(bookDtoList, booksDto.getBooks());
    }
}
