package com.example.bookstore.scheduled;

import com.example.bookstore.payloads.BookDto;
import com.example.bookstore.payloads.BooksDto;
import com.example.bookstore.services.BookService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.TestPropertySource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.mockito.Mockito.*;

@SpringBootTest
@TestPropertySource(properties = "scheduled.cron.sync-book.expression=*/2 * * * * *")
public class CronSchedulerTests {

    @SpyBean
    CronScheduler scheduler;

    @Mock
    BookService service;

    @Test
    public void whenWaitTenSeconds_thenScheduledIsCalledAtLeastThrice() {
        List<BookDto> list = new ArrayList<>();
        list.add(
                BookDto
                        .builder()
                        .id(1)
                        .book("book a")
                        .author("author a")
                        .price(BigDecimal.valueOf(100.25))
                        .isRecommended(false)
                        .build());

        when(service.getBooks()).thenReturn(
                BooksDto
                        .builder()
                        .books(list)
                        .build());

        await().atMost(10, TimeUnit.SECONDS)
                .untilAsserted(() -> verify(scheduler, atLeast(3))
                        .syncBookTask());
    }
}
